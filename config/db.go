package config


import (
	)
func dbConn() (db *sql.DB) {

    dbDriver := "mysql"
    dbUser := "root"
    dbPass := "root"
    dbName := "ashish"
    db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
    if err != nil {
        panic(err.Error())
    }
    return db
}